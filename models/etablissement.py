from elasticsearch_dsl import InnerDoc, Text, Integer, Keyword


class Etablissement(InnerDoc):
    siren = Integer()
    codeEffectif = Text(fields={'keyword': Keyword()})
    codeActivite = Text(fields={'keyword': Keyword()})
    codeNaf = Text()

