import pandas as pd
from elasticsearch_dsl import Text, GeoShape, Nested, InnerDoc, Keyword
from models.equipement import Equipement


class Iris(InnerDoc):
    dcomIris = Text(fields={'keyword': Keyword()})
    geometry = GeoShape()
    equipements = Nested(Equipement)

    def add_equipments(self):
        df = pd.read_csv('FILES/bpe18_ensemble.csv', delimiter=';', usecols=['DCIRIS', 'TYPEQU', 'NB_EQUIP'])
        condition = df['DCIRIS'] == self.dcomIris
        data_to_add = df[condition]

        for index, row in data_to_add.iterrows():
            equipement = Equipement(typeEquipement=row['TYPEEQU'], nbEquipement=row['NB_EQUIP'])
            self.equipements.append(equipement)

