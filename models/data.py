from elasticsearch_dsl import Document, Text, Object, Keyword
from models.commune import Commune


class Data(Document):
    nomActivite = Text(fields={'keyword': Keyword()})
    commune = Object(Commune)

    class Index:
        name = 'intencite'
