from elasticsearch_dsl import InnerDoc, Text, Integer


class Equipement(InnerDoc):
    typeEquipement = Text(required=True)
    nbEquipement = Integer(required=True)


