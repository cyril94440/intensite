import geopandas as gpd
import pandas as pd
from elasticsearch_dsl import Text, Integer, Nested, InnerDoc, Keyword
from models.iris import Iris
from models.etablissement import Etablissement

cols = ['siren', 'trancheEffectifsEtablissement',
        'activitePrincipaleEtablissement', 'nomenclatureActivitePrincipaleEtablissement']


class Commune(InnerDoc):
    nomCommune = Text(fields={'keyword': Keyword()})
    departement = Text()
    region = Integer()
    codePostal = Text()
    population = Integer()
    densite = Integer()
    iris = Nested(Iris)
    etablissements = Nested(Etablissement)

    def add_iris(self):
        df_iris = gpd.read_file("FILES/iris-2013-01-01/iris-2013-01-01.shp")
        df_iris = df_iris.drop_duplicates(subset='DCOMIRIS', keep='first')
        condition = df_iris['NOM_COM'] == self.nomCommune
        df_iris = df_iris[condition]

        for index, row in df_iris.iterrows():
            i = Iris(dcomIris=row['DCOMIRIS'], geometry=row.geometry.wkt)
            i.add_equipments()
            self.iris.append(i)

    def add_company(self):
        df_company = pd.read_csv("FILES/etablissements_argenteuil.csv", usecols=cols)
        for index, row in df_company.iterrows():
            company = Etablissement(siren=row['siren'], codeEffectif=row['trancheEffectifsEtablissement'],
                                    codeActivite=row['activitePrincipaleEtablissement'],
                                    codeNaf=row['nomenclatureActivitePrincipaleEtablissement'])
            self.etablissements.append(company)

