import pandas as pd
from models.data import Data
from models.commune import Commune
from elasticsearch_dsl import connections
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk


def setup():
    """ Create an IndexTemplate and save it into elasticsearch. """
    index_template = Data._index.as_template('base')
    index_template.save()


es = Elasticsearch()

connections.create_connection(hosts=['35.205.69.3:9200'])



Data.init()

df = pd.read_csv('FILES/webtransfer-360421/densite-pop-commune.csv', delimiter=';')
df = df.drop_duplicates()
condition = df['Commune'] == 'Argenteuil'
df = df[condition]
row = df.iloc[0, :]

obj = Commune(
    nomCommune=row['Commune'].upper(),
    departement=row['Dep'],
    region=row['Reg'],
    codePostal=row['CP'],
    population=row['Population'],
    densite=row['Densite']
)

d = Data(nomActivite='Boulangerie', commune=obj)

add_company()

d.save()
